# README #

This is a LaTeX docker to have the most general setup for my LaTeX projects
I usually use texlive, together with scripts to convert svg2pdf.
For my school lessons, I have some exercises which were done in libreoffice, and this also gets converted to PDF. Therefore, the libreoffice programs are also part of this docker setup (sorry for the big file ...) If you don't need libreoffice, have a look a [latex docker](https://hub.docker.com/r/daesters/latex).
The script is based on the python [cairosvg](https://cairosvg.org/documentation/) converter, which converts SVG files to PDF (and others)

### What is this repository for? ###

* LaTeX compilation
* Version 1.0

### How do I get set up? ###

* Basic docker stuff

### Contribution guidelines ###

* Almost always appriciate (see points below)
* Be nice
* Don't blow up code ;-)

### Who do I talk to? ###

* Blaims: Me
* Docker problems: Docker community
* Faith issues: Thy Father in heaven
